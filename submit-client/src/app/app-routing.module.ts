import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompleteRegistrationComponent } from './auth/complete-registration/complete-registration.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { CoursesComponent } from './entities/courses/courses.component';
import { HomeComponent } from './entities/home/home.component';

const routes: Routes = [
  {
    path:'',
    pathMatch:'full',
    redirectTo:'login'

  },
  // {
  //   path:'home',
  //   component:HomeComponent
  // }, 
  {
    path:'register', 
    component:RegisterComponent
  },
  {
    path:'finish-account-setup',
    component:CompleteRegistrationComponent
  },
  {
    path:'login',
    component:LoginComponent
  }, 
  {
    path:"course",
    component:CoursesComponent
  },
  {
    path:'reset-password',
    component:ResetPasswordComponent
  },
  {
    path:'request-password-reset',
    component:ForgotPasswordComponent
  }
 
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
