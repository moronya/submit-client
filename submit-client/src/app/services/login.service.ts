import { HttpClient } from '@angular/common/http';
// import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Inject, Injectable } from '@angular/core';
import { Constants } from '../Constants';
import  Jwt  from 'jwt-decode'


@Injectable({
  providedIn: 'root'
})
export class LoginService {


  constructor(
    private http:HttpClient,
    // @Inject (LOCAL_STORAGE) private localStorage:StorageService,
  ) { }

  USER_KEY = 'user';

  loginUser(data:any){
    return this.http.post(Constants.API_ENDPOINT+ "auth", data, {responseType: 'text'})
  }

  //decode JWT key
  decodeJWT(token: string){
    return Jwt(token);
  }

  //get user from storage
  getUserFromStorage(){
    const user:any = localStorage.getItem(this.USER_KEY);
    return JSON.parse(user);
  }

  //forgot password
  forgotPassword(data:any){
    return this.http.post(Constants.API_ENDPOINT + "request-password-reset", data, {responseType: 'text'})
  }

  //reset password
  resetPassword(data:any){
    return this.http.put(Constants.API_ENDPOINT+ "reset-password", data, {responseType: 'text'})
  }
  public logoutUser(){
    return localStorage.removeItem('token')
  }

  //remove user
  removeUser(){
    return localStorage.removeItem(this.USER_KEY);
  }

 
}
