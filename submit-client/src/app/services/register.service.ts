import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../Constants';



@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient
  ) { }
  registerUser(data:any){
    return this.http.post(Constants.API_ENDPOINT+"register/user", data, {responseType: 'text'})
  }
  
  completeUserRegistration(data:any){
    return this.http.post(Constants.API_ENDPOINT+"complete-user-registration", data, {responseType: 'text'})
  }

}
