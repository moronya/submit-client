import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../Constants';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(
    private http:HttpClient
  ) { }

  createCourse(data:any){
    return this.http.post(Constants.API_ENDPOINT +"create-course", data, {responseType : 'text'})
  }

  getCoursesByUserEmail(email:string): Observable<any>{
    return this.http.get(Constants.API_ENDPOINT+"fetch-courses/" + email);
  }
}
