import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './entities/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompleteRegistrationComponent } from './auth/complete-registration/complete-registration.component';
import { LoginComponent } from './auth/login/login.component';
import { CoursesComponent } from './entities/courses/courses.component';
import { NavbarComponent } from './entities/navbar/navbar.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';



@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    CompleteRegistrationComponent,
    LoginComponent,
    CoursesComponent,
    NavbarComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule,
    ReactiveFormsModule, 
    HttpClientModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
