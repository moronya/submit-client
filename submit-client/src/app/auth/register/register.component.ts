import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  // @ViewChild('tab') tab: ElementRef;

  registerData:any


  constructor(  
    private formBuilder:FormBuilder,
    private registerService:RegisterService, 
    private router:Router
  ) {
    this.registerData = this.formBuilder.group({
      role:[''],
      email:['']
    });
    
   }
  // Initial user registation
  registerUser(){
    console.log("User data: ", this.registerData)
   

    this.registerService.registerUser(this.registerData.value).subscribe(
      (res)=>{
        //success
        console.log("User has been registered: ", res)
       
        //redirect user to complete account setup
        this.router.navigate(['finish-account-setup']);

      },
      (err) =>{
        //error      
        console.log("Failed to register the user: ", err)       

      }
    )

  }
  

  ngOnInit(): void {
  }

  


}
