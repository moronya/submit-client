import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CoursesService } from 'src/app/services/courses.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginData:any
  userData:any
  isLoggedIn! : boolean
  constructor(
    private formBuilder:FormBuilder, 
    private loginService:LoginService,
    private router:Router,
    private courseService:CoursesService
  ) {
    this.loginData = this.formBuilder.group({
      email:[''],
      password:['']
    })
   }

  ngOnInit(): void {
  }

  loginUser(){
    //login user
    this.loginService.loginUser(this.loginData.value).subscribe(
      (res) => {
        this.saveTokenToStorage(res)        
         //decode JWT
         this.userData = this.loginService.decodeJWT(res);
         console.log("Logged in user data: ", this.userData);
         //save token
        
         this.isLoggedIn = true

         const role = this.userData.auth

         //route user to page according to their role.
         this.route(role, this.userData.sub) 
      },
      (err)=> {
        console.log("Failed to login the user: ", err)
        this.loginData.reset();

      }
    )

  }

  //save token to storage

  saveTokenToStorage(token : string){
    localStorage.setItem("token", token);
  }

  //routing user based on their role
  route(role:string, email?:any){
    if (role == "INSTRUCTOR"){
      this.courseService.getCoursesByUserEmail(email).subscribe(
        (res) => {
          console.log("Retrieved courses ... ", res)
          this.router.navigate(['course'])
        },

        (err) =>{
          console.log("Failed to retrieve courses ...", err)
        }
      )
    
    }

    else if (role == "STUDENT"){
      this.courseService.getCoursesByUserEmail(email).subscribe(
        (res) => {
          console.log("Retrieved courses ... ", res)
          this.router.navigate(['course'])
        },

        (err) =>{
          console.log("Failed to retrieve courses ...", err)
        }
      )
    
    }

  }

}
