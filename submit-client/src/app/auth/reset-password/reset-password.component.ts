import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
userData:any

  constructor(
    private formBuilder:FormBuilder,
    private loginService:LoginService,
    private router:Router
  ) {
    this.userData = this.formBuilder.group(
      {
        resetKey:[''],
        newPassword: ['']
      }
    )

   }

  ngOnInit(): void {
  }

  //reset password
  resetPassword(){
    this.loginService.resetPassword(this.userData.value).subscribe(
      (res) =>{
        //success
        this.router.navigate(['login'])

      },
      (err) => {
        //fail
      }
    )
  }

}
