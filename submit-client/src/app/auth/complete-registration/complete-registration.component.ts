import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-complete-registration',
  templateUrl: './complete-registration.component.html',
  styleUrls: ['./complete-registration.component.scss']
})
export class CompleteRegistrationComponent implements OnInit {
  userData:any;

  constructor(
    private registerService:RegisterService,
    private formBuilder:FormBuilder,
    private router:Router
  ) { 
    this.userData = this.formBuilder.group({
      resetKey:[''],
      password:[''],  
      
    })
  }

  ngOnInit(): void {
  }

  
  completeUserRegistration(){
    console.log("Complete User data: ", this.userData)

    this.registerService.completeUserRegistration(this.userData.value).subscribe(
      () =>{
        //success
        this.router.navigate(['/login'])
      },
      () => {
        //err
      }
    )
  }


}
