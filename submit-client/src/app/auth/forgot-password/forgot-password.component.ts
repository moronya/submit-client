import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  userData:any

  constructor(
    private formBuilder:FormBuilder,
    private loginService:LoginService,
    private router:Router
  ) { 
    this.userData = this.formBuilder.group(
      {
        email : ['']
      }
    )
  }

  ngOnInit(): void {
  }

  requestPasswordReset(){
    this.loginService.forgotPassword(this.userData.value).subscribe(
      (res) =>{
        //success
        this.router.navigate(['reset-password'])
      },
      (err) =>{
      //error
      }
    )
  }

}
