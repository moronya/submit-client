import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit { 
  isLoggedIn!:boolean
  
user:any
  constructor(
    private loginService:LoginService,
    private router:Router
  ) { 

    //check for user from local storage every time url changes
    router.events.forEach((event) =>{
      if(event instanceof NavigationStart){

        if(loginService.getUserFromStorage != null){
          this.user
          this.isLoggedIn = true
        }else{
          this.isLoggedIn = false
        }
      }
       
    })
   
  }

  ngOnInit(): void {
   

  }
  //check which user is logged in

  //logout user

logoutUser(){
this.isLoggedIn = false
 //remove token from local storage
 this.loginService.logoutUser()

 //remove user from storage
 this.loginService.removeUser()

 //redirect user to Login page
 this.router.navigate(['/login'])


}


}
