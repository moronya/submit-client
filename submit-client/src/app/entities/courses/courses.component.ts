import { tokenize } from '@angular/compiler/src/ml_parser/lexer';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavigationStart, Router } from '@angular/router';
import { CoursesService } from 'src/app/services/courses.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courseData: any
  courseInfo: any
  user: any



  constructor(
    private formBuilder: FormBuilder,
    private courseService: CoursesService,
    private loginService: LoginService,
    private router: Router
  ) {
    this.courseData = this.formBuilder.group(
      {
        courseCode: [''],
        courseName: ['']
      }
    )

    // check for user from local storage every time url changes
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.user = this.loginService.getUserFromStorage();
      }
    })

  }


  ngOnInit(): void {
    this.getCourses();

  }

  createCourse() {
    this.courseService.createCourse(this.courseData.value).subscribe(
      (res) => {
        console.log("Result : ", res)
      },

      (err) => {
        console.log("Failed to Create Course: ", err)
      }

    )
  }

  getCourses() {
    const token: any = localStorage.getItem("token")
    var userData: any = this.loginService.decodeJWT(token);
    var email = userData.sub

    this.courseService.getCoursesByUserEmail(email).subscribe(
      (res) => {
        console.log("Retrieved courses ...")
        this.courseInfo = res
        //fetched all courses
      },

      (err) => {
        console.log("Failed to retrieve courses : ", err);
        //failed to fetch all courses

      }
    )

  }




}
